using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Player {

	public int primeiraColisao = -1;
	/*
	 * Classe que armazena informações do jogador
	*/

	private IList<Int32> bolasConvertidas = new List<Int32>();
	public bool estaNaOito;

	public Player(string name) {
		Name = name;
		estaNaOito = false;
		TipoDeBola = TipoBola.Nenhuma;
	}

	public string Name {
		get;
		private set;
	}

	public TipoBola TipoDeBola;

	public int Pontos {
		get { return bolasConvertidas.Count; }
	}

	public void BolaConvertida(int numeroDaBola) {
		Debug.LogWarning("JOGADOR: " + Name + " converteu a bola " + numeroDaBola);

		bolasConvertidas.Add(numeroDaBola);
        if (bolasConvertidas.Count == 7)
        {
            Debug.LogWarning(Name + " está no bola 8");
            estaNaOito = true;
        }
	}

    internal void RetiraBola()
    {
        if (this.TipoDeBola == TipoBola.Lisa)
        {
            if (this.estaNaOito)
            {
                // VC ganhou - PARA Fazer
                GameObject.Destroy(GameObject.Find("Bola_08"));
                return;
            }
            for (int b = 1; b < 8; b++)
            {
                if (!bolasConvertidas.Contains(b))
                {
                    this.BolaConvertida(b);
                    // Destrui bola
                    GameObject.Destroy(GameObject.Find("Bola_" + b.ToString().PadLeft(2, '0')));
                    return;
                }
            }
        }
        else if (this.TipoDeBola == TipoBola.Listrada)
        {
            if (this.estaNaOito)
            {
                // VC ganhou - PARA Fazer
                GameObject.Destroy(GameObject.Find("Bola_08"));
                return;
            }
            for (int b = 9; b < 16; b++)
            {
                if (!bolasConvertidas.Contains(b))
                {
                    this.BolaConvertida(b);
                    // Destrui bola
                    GameObject.Destroy(GameObject.Find("Bola_" + b.ToString().PadLeft(2,'0')));
                    return;
                }
            }
        }
    }
}
