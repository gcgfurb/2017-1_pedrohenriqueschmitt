﻿using UnityEngine;
using System.Collections;

public enum TipoBola
{
	Listrada, Lisa, Nenhuma
}

public class BolaController : MonoBehaviour {

	// FONTE ANTIGO
	void FixedUpdate () {
		var rigidbody = GetComponent<Rigidbody>();
		if (rigidbody.velocity.y > 0) {
			var velocity = rigidbody.velocity;
			velocity.y *= 0.3f;
			rigidbody.velocity = velocity;
		}
	}// FONTE ANTIGO

	private Rigidbody rb;
	// Use this for initialization
	void Start () {
		rb = transform.GetComponent<Rigidbody>();

		// Fonte antigo - Verificar o que faz
		GetComponent<Rigidbody>().sleepThreshold = 0.15f;
	}

	// Update is called once per frame
	void Update () {

		//Faz com que a bola pare quando estiver a uma velocidade baixa
		if (name == "Bola_Branca") {
			/*if (rb.velocity.magnitude > 0)
				Debug.Log ("===========================================================" + name + "-- " + rb.velocity.magnitude);
			else
				Debug.Log ("===========================================================" + name + "-- 0");
		*/}
		if(rb.velocity.magnitude < 0.4 && rb.velocity.magnitude > 0){
			rb.velocity = new Vector3(0,0,0);
		}

		//Evita que a bola pule na mesa
		if(transform.position.y > 0.54f){
			transform.position = new Vector3(transform.position.x,0.54f,transform.position.z);
		}
	}
}
