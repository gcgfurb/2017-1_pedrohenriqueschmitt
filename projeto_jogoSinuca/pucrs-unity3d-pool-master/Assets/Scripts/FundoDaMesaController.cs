﻿using UnityEngine;
using System.Collections;

public class FundoDaMesaController : MonoBehaviour {
	public GameObject bolas;
	public GameObject bolaBranca;

	private Vector3 posicaoInicial_BolaBranca;

	void Start() {
		posicaoInicial_BolaBranca = bolaBranca.transform.position;
	}

	void OnCollisionEnter(Collision collision) {
		// Verifica se as bolas foram encaçapadas
		foreach (var transform in bolas.GetComponentsInChildren<Transform>()) {
			if (transform.name == collision.gameObject.name) {
				var objectName = collision.gameObject.name;
				//Destrui o bola convertida
				GameObject.Destroy(collision.gameObject);

				var numeroBola = int.Parse(objectName.Replace("Bola_", ""));
				// Informa ao controlador que a bola foi convertida
				PoolGameController.GameInstance.BolasConvertidas(numeroBola);
			}
		}

		// Se foi a bola branca, volta para posição inicial
		if (bolaBranca.transform.name == collision.gameObject.name) {
			bolaBranca.transform.GetComponent<Rigidbody> ().velocity = new Vector3 (0, 0, 0);
			bolaBranca.transform.position = posicaoInicial_BolaBranca;
		}
	}
}
