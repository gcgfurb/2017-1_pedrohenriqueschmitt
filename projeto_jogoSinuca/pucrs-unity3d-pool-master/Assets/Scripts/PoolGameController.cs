﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class PoolGameController : MonoBehaviour {

	/*
	 * Classe que controla tudo e pode ser chamado em qualquer momento pelo método GameInstance
	*/
	public GameObject taco;
	public GameObject bolaBranca;
	public GameObject bolas;
	//public GameObject scoreBar;
	public GameObject winnerMessage; // Mensagem no meio da tela
	public GameObject winnerMessage2; // Mensagem no meio da tela

	public float maxForce;
	public float minForce;
	public Vector3 direcaoDoTaco;

	public const float MIN_DISTANCE = 27.5f;
	public const float MAX_DISTANCE = 32f;
	
	public IGameObjectState estadoAtual;

	public Player CurrentPlayer; // Jogador atual
	public Player OtherPlayer; // Segundo jogador
    private bool definidoOsGruposDeBolas; // Informa se já foi realizado a definicao

	private bool currentPlayerContinuesToPlay = false;
	private IList<Int32> bolasConvertidasNaTacada = new List<Int32>();

	static public PoolGameController GameInstance {
		get;
		private set;
	}

	void Start() {
		direcaoDoTaco = Vector3.forward;
		CurrentPlayer = new Player("Pedro");
		OtherPlayer = new Player("João");
        definidoOsGruposDeBolas = false;

		GameInstance = this;

		// Mensagem fica invisivel
		winnerMessage.GetComponent<Canvas>().enabled = false;
		winnerMessage2.GetComponent<Canvas>().enabled = false;

		estadoAtual = new GameStates.DirecionandoTacoState(this);
	}
	
	void Update() {
		estadoAtual.Update();
	}

	void FixedUpdate() {
		estadoAtual.FixedUpdate();
	}

	void LateUpdate() {
		estadoAtual.LateUpdate();
	}

	public void VerificaRegrasDaTacada(){
		/// REVER Se bater primeiro na bola OITO deve levar punição, mas apenas se não tiver na bola 8
		// REVER se levar punição, perde o direito de tacar e perde uma bola, porém se matou uma bola do adversario na tacada, a punição já está paga

		// REGRA 01 - A bola branca deve bater primeiro na bola do seu grupo
		if (CurrentPlayer.primeiraColisao == -1 ||
            (CurrentPlayer.TipoDeBola == TipoBola.Lisa && CurrentPlayer.primeiraColisao > 8 && definidoOsGruposDeBolas) ||
            (CurrentPlayer.TipoDeBola == TipoBola.Listrada && CurrentPlayer.primeiraColisao < 8 && definidoOsGruposDeBolas) ||
			(CurrentPlayer.primeiraColisao == 8 && !CurrentPlayer.estaNaOito)) {

			currentPlayerContinuesToPlay = false; // Rever todos os lugares que preenche esse campo, pois deve ser verificado ao final da tacada, verifica a lista de bolas convertidas para escolher se continua ou não

			// Verifica se precisa pagar bola, ou seja já foi paga na tacada realizada
			if (CurrentPlayer.TipoDeBola == TipoBola.Lisa && !verificaSeTemBolaDoGrupo(TipoBola.Listrada, bolasConvertidasNaTacada)) {
				// Paga bola
                if (OtherPlayer.estaNaOito)
                {
                    FimDeJogo(OtherPlayer);
                }
                OtherPlayer.RetiraBola();
			}
			if (CurrentPlayer.TipoDeBola == TipoBola.Listrada && !verificaSeTemBolaDoGrupo( TipoBola.Lisa, bolasConvertidasNaTacada)) {
				// Paga bola
                if (OtherPlayer.estaNaOito)
                {
                    FimDeJogo(OtherPlayer);
                }
                OtherPlayer.RetiraBola();
			}
		}

        if (!definidoOsGruposDeBolas)
        {
            if (CurrentPlayer.TipoDeBola != TipoBola.Nenhuma)
                definidoOsGruposDeBolas = true;
        }
	}

	public bool verificaSeTemBolaDoGrupo(TipoBola tipoBola, IList<int> bolas){
		foreach (Int32 bola in bolas) {
			if (tipoBola == TipoBola.Lisa && bola < 8)
				return true;
			if (tipoBola == TipoBola.Listrada && bola > 8)
				return true;
		}
		return false;
	}

	public void BolasConvertidas(int numeroDaBola) {
		bolasConvertidasNaTacada.Add (numeroDaBola);

		DefineGrupoDeBolasDosJogadores (numeroDaBola);


		if (numeroDaBola < 8) {
			switch (CurrentPlayer.TipoDeBola) {
				case TipoBola.Lisa:
				{
					currentPlayerContinuesToPlay = true;
					CurrentPlayer.BolaConvertida(numeroDaBola);
					break;
				}
				case TipoBola.Listrada:
				{
					// PErdeu
					OtherPlayer.BolaConvertida(numeroDaBola);
					break;
				}
			}
		} else if (numeroDaBola > 8) {
			switch (CurrentPlayer.TipoDeBola) {
			case TipoBola.Listrada:
				{
					currentPlayerContinuesToPlay = true;
					CurrentPlayer.BolaConvertida(numeroDaBola);
					break;
				}
			case TipoBola.Lisa:
				{
					// PErdeu
					OtherPlayer.BolaConvertida(numeroDaBola);
					break;
				}
			}
        }
        else if (numeroDaBola == 8)
        {
            if (CurrentPlayer.estaNaOito && !verificaSeTemBolaDoGrupo(CurrentPlayer.TipoDeBola, bolasConvertidasNaTacada))
            {
                FimDeJogo(CurrentPlayer);
            }
            else
            {
                FimDeJogo(OtherPlayer);
            }
        }
	}

	private void DefineGrupoDeBolasDosJogadores(int numeroDaBola){
		if (CurrentPlayer.TipoDeBola == TipoBola.Nenhuma) {
			if (numeroDaBola > 8) {
				CurrentPlayer.TipoDeBola = TipoBola.Listrada;
				OtherPlayer.TipoDeBola = TipoBola.Lisa;
			} else if (numeroDaBola < 8) {
				CurrentPlayer.TipoDeBola = TipoBola.Lisa;
				OtherPlayer.TipoDeBola = TipoBola.Listrada;
			}
			Debug.LogWarning(CurrentPlayer.Name + " -> " + CurrentPlayer.TipoDeBola.ToString());
			Debug.LogWarning(OtherPlayer.Name + " -> " + OtherPlayer.TipoDeBola.ToString());
		}
	}

	public void NextPlayer() {

		if (currentPlayerContinuesToPlay) {
			currentPlayerContinuesToPlay = false;
			MostrarMensagem2 (CurrentPlayer.Name + " continua jogando...");
			Debug.LogWarning (CurrentPlayer.Name + " continua jogando...");
		} else {
			MostrarMensagem2 (OtherPlayer.Name + " é a sua vez.");
			Debug.LogWarning (OtherPlayer.Name + " é a sua vez.");

			#region Troca de jogador
			var aux = CurrentPlayer;
			CurrentPlayer = OtherPlayer;
			OtherPlayer = aux;
			#endregion
		}
		CurrentPlayer.primeiraColisao = -1;
		bolasConvertidasNaTacada = new List<Int32> (); // Limpa lista de bolas convertidas na tacada
	}

	public void MostrarMensagem2(string txt)
	{
		var text = winnerMessage2.GetComponentInChildren<UnityEngine.UI.Text>();
		text.text = txt;
		// Deixa a mensagem visivel
		winnerMessage2.GetComponent<Canvas>().enabled = true;

		StartCoroutine ("Espera");
	}

	IEnumerator Espera(){
		yield return new WaitForSeconds (2.0f);
		winnerMessage2.GetComponent<Canvas>().enabled = false;
	}

	public void FimDeJogo(Player vencedor) {
		var msg = "Fim de jogo\n";

		msg += string.Format("'{0}' ganhou a partida", vencedor.Name);
		
		var text = winnerMessage.GetComponentInChildren<UnityEngine.UI.Text>();
		text.text = msg;
		// Deixa a mensagem visivel
		winnerMessage.GetComponent<Canvas>().enabled = true;
	}
}
