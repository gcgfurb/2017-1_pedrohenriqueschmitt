using UnityEngine;
using System;

namespace GameStates {

	/*
	 * Chamado quando o usuário solta o botão do mouse para realizar a tacada.
	*/

	public class TacadaState : AbstractGameObjectState {
		private PoolGameController gameController;
		
		private GameObject taco;
		private GameObject bolaBranca;

		private float speed = 5f;
		private float force = 0f;
		
		public TacadaState(MonoBehaviour parent) : base(parent) { 
			gameController = (PoolGameController)parent;
			taco = gameController.taco;
			bolaBranca = gameController.bolaBranca;

			var forceAmplitude = gameController.maxForce - gameController.minForce;
			var relativeDistance = (Vector3.Distance(taco.transform.position, bolaBranca.transform.position) - PoolGameController.MIN_DISTANCE) / (PoolGameController.MAX_DISTANCE - PoolGameController.MIN_DISTANCE);
			force = forceAmplitude * relativeDistance + gameController.minForce;
		}

		public override void FixedUpdate () {
			var distance = Vector3.Distance(taco.transform.position, bolaBranca.transform.position);
			if (distance < PoolGameController.MIN_DISTANCE) {
				bolaBranca.GetComponent<Rigidbody>().AddForce(gameController.direcaoDoTaco * force);
				taco.GetComponent<Renderer>().enabled = false;
                taco.SetActive(false);
				taco.transform.Translate(Vector3.down * speed * Time.fixedDeltaTime);
				gameController.estadoAtual = new GameStates.WaitingForNextTurnState(gameController);
			} else {
				taco.transform.Translate(Vector3.down * speed * -1 * Time.fixedDeltaTime);
			}
		}
	}
}