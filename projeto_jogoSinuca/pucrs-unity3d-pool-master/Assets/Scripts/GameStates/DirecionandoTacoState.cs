﻿using UnityEngine;
using System.Collections;

namespace GameStates {

	/*
	 * Cuida do movimento do taco para o lado ou para o outro lado. 
	 * Ou seja, verifica se o usuário está mudando a direção do taco em relação a bola branca.
	*/

	public class DirecionandoTacoState  : AbstractGameObjectState {
		private GameObject taco;
		private GameObject bolaBranca;

		private PoolGameController gameController;

		public DirecionandoTacoState(MonoBehaviour parent) : base(parent) { 
			gameController = (PoolGameController)parent;
			taco = gameController.taco;
			bolaBranca = gameController.bolaBranca;

			taco.GetComponent<Renderer>().enabled = true;//Visivel
            taco.SetActive(true);
		}

		public override void Update() {
			var movimentoDoTaco = Input.GetAxis("Horizontal");

			if (movimentoDoTaco != 0) {
				// Calculo o angulo
				var angle = movimentoDoTaco * 75 * Time.deltaTime;
				// Atualiza a direcao do taco
				gameController.direcaoDoTaco = Quaternion.AngleAxis(angle, Vector3.up) * gameController.direcaoDoTaco;
				// Posiciona o taco na direção correta em relação a bola branca
				taco.transform.RotateAround(bolaBranca.transform.position, Vector3.up, angle);
			}
			//Debug.DrawLine(bolaBranca.transform.position, bolaBranca.transform.position + gameController.direcaoDoTaco * 10);

			if (Input.GetButtonDown("Fire1")) { // Clicou no botão do mouse
				gameController.estadoAtual = new GameStates.ForcaDoTacoState(gameController);
			}
		}
	}
}