﻿using UnityEngine;
using System.Collections;

namespace GameStates {

	/*
	 * Chamado quando o usuário clica no mouse para calcular a força da tacada.
	 * Faz o movimento de vai e volta do taco em relação a bola branca
	*/

	public class ForcaDoTacoState : AbstractGameObjectState {
		private PoolGameController gameController;

		private GameObject taco;
		private GameObject bolaBranca;

		private float cueDirection = -1;
		private float speed = 5;

		public ForcaDoTacoState(MonoBehaviour parent) : base(parent) { 
			gameController = (PoolGameController)parent;
			taco = gameController.taco;
			bolaBranca = gameController.bolaBranca;
		}

		public override void Update() {
			// Soltou o botão do mouse
			if (Input.GetButtonUp("Fire1")) {
				gameController.estadoAtual = new GameStates.TacadaState(gameController);
			}
		}

		public override void FixedUpdate () {
			// Calcula a distancia entre o taco e a bola
			var distance = Vector3.Distance(taco.transform.position, bolaBranca.transform.position);
			if (distance < PoolGameController.MIN_DISTANCE || distance > PoolGameController.MAX_DISTANCE)
				cueDirection *= -1;// Muda de sinal para o taco ir e voltar
			taco.transform.Translate(Vector3.down * speed * cueDirection * Time.fixedDeltaTime);
		}
	}
}