﻿using System;
using UnityEngine;

namespace AssemblyCSharp
{
	public class Direcao
	{
		public static bool alterouDirecao(Vector3 direcao1, Vector3 direcao2){
			float difX = direcao1.x - direcao2.x;
			if (difX > 0.1f || difX < -0.1f)
				return true;

			float difZ = direcao1.z - direcao2.z;
			if (difZ > 0.1f || difZ < -0.1f)
				return true;
			
			return false;
		}

		public static Vector3 getDirecao(Vector3 posicao1, Vector3 posicao2){
			var dir = posicao2 - posicao1;
			var direction = dir / dir.magnitude;
			return direction;
		}

		public static Vector3 getDirecao(Vector3 rotation){
			Vector3 retorno = new Vector3 (0, 0, 0);
			float angulo = rotation.y; // 0 a 360

			//Descobrir o quadrante dele
			if (angulo >= 0 && angulo <= 90) {
				// Quad. 1
				retorno = calculaDirecaoPeloAngulo(0f,-1f,90,angulo);
				float z = retorno.z;
				retorno.z = retorno.x;
				retorno.x = z * -1;

			} else if (angulo > 90 && angulo <= 180) {
				// Qud. 2
				retorno = calculaDirecaoPeloAngulo(0f,-1f,180,angulo);
			} else if (angulo > 180 && angulo < 270) {
				// Qud. 3
				retorno = calculaDirecaoPeloAngulo(-1f,0f,270,angulo);
				float z = retorno.z;
				retorno.z = retorno.x;
				retorno.x = z * -1;

			} else {
				// Qud. 4
				retorno = calculaDirecaoPeloAngulo(0f,1f,360,angulo);
			}
			//Debug.Log ("Direção: " + retorno);
			return retorno;
		}

		private static Vector3 calculaDirecaoPeloAngulo(float inicial, float final, float maxLimiteQuad, float angulo){
			Vector3 retorno = new Vector3 (0, 0, 0);
			float dif = final - inicial;
			float angY = maxLimiteQuad - angulo;
			if (angY == 90)
				retorno.z = dif;
			else
				retorno.z = dif / (90 / angY);
			retorno.x = dif - retorno.z;
			return retorno;
		}
	}
}

