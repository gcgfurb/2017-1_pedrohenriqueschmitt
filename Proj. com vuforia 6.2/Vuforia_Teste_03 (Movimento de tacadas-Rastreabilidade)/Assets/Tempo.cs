﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tempo : MonoBehaviour {

    Image fillImg;
    public bool iniciar;

    private float current;
    public float speed;

    // Use this for initialization
    void Start()
    {
        fillImg = this.GetComponent<Image>();
  	}

    // Update is called once per frame
    void Update()
    {
        if (iniciar)
        {
            if (current < 100)
            {
				current += (speed * Time.deltaTime) * 2; // O 2 é para ser mais rapido
            }
            else
            {
                Debug.Log("OK tempo finalizado!");
				GameObject.FindWithTag ("Player").GetComponent<Rigidbody> ().velocity = new Vector3 (0f, 0f, 0f);
				GameObject.FindWithTag ("Player").transform.position = new Vector3 (-0.22f, 107.6555f, 0f);
				GameObject.FindWithTag ("Player").GetComponent<Rigidbody> ().freezeRotation = true;
				GameObject.FindWithTag ("Player").GetComponent<Rigidbody> ().freezeRotation = false;
				this.iniciar = false;
            }
            fillImg.fillAmount = current / 100;
        }
        else
        {
            fillImg.fillAmount = 0;
            current = 0;
        }
    }
}
