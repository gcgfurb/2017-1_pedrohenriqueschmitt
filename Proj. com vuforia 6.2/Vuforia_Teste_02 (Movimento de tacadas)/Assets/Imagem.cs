﻿using UnityEngine;
using System.Collections;
using Vuforia;

public class Imagem : MonoBehaviour, ITrackableEventHandler {
	public int tempo;
	private int contador;
	private TrackableBehaviour mTrackableBehaviour;
	public Material verde;

	void Start()
	{
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
		contador = 0;
	}

	private void CriarPonto(Vector3 position){
		var cube = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		cube.transform.position = position; //new Vector3 (3f, 100f, 0f);
		cube.transform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
		Renderer r = cube.GetComponent<Renderer> ();
		if (r != null) {
			r.material = verde;
		}
	}

	// Update is called once per frame
	void Update () {
		if (tempo == contador) {
			contador = 0;
			Debug.Log ("ffffffffffffffffffffffffff");
		}
		contador++;
		CriarPonto (this.transform.position);
	}

	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			Debug.Log ("Encontrou");
		}
		else
		{
			Debug.Log ("PErdeu");
		}
	}
}
